<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'img',
        'stock',
        'base_price',
        'factory_price',
        'price_with_tax',
        'fee_gain',
    ];
}
