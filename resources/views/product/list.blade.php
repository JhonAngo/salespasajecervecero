@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-10">
                <h2>Catalogo de Productos</h2>
        </div>
        <div class="col-lg-2">
            <a class="btn btn-success" href="{{ route('products.create') }}">Añadir Nuevo</a>
        </div>
    </div>
 
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
 
    <table class="table table-dark">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>Stock</th>
            <th>Ganancia</th>
            <th width="280px">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->fee_gain }}</td>
                <td>
                    <form action="{{ route('products.index',$product->id) }}" method="POST">
                        <!-- <a class="btn btn-info" href="{{ route('products.index',$product->id) }}">Show</a> -->
                        <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Editar</a>
                        <a class="btn btn-danger" href="{{ route('products.delete',$product->id) }}">Eliminar</a>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection