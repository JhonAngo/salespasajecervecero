@extends('adminlte::page')

@section('content')

<div class="row">
        <div class="col-lg-10">
            <h2>Agregar nuevo producto</h2>
        </div>
        <div class="col-lg-2">
            <a class="btn btn-warning" href="{{ route('products.index') }}"> Regresar</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>:(</strong> Existe un error.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('products.update',$product->id) }}" method="POST">
    @csrf
    @method('PUT')
        <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Nombre:</strong>
                        <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Nombre">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="form-group">
                        <strong>Stock:</strong>
                        <input type="text" name="stock" value="{{ $product->stock }}" class="form-control" placeholder="Stock">
                    </div>
                </div>
        </div>
    
        <div class="col-lg-10">
            <h3>Precios</h3>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong>Fabrica:</strong>
                    <input type="number" step="0.01" value="{{ $product->factory_price }}" id="factory_price"  name="factory_price" class="form-control" placeholder="0.00">
                </div>
            </div>
            <div class="col">
                <strong>Publico:</strong>
                <input type="number" step="0.01" value="{{ $product->price_with_tax }}" id="price_with_tax" name="price_with_tax" class="form-control" placeholder="0.00">
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong>Sin IVA:</strong>
                    <input type="number" step="0.01" value="{{ $product->base_price }}" id="base_price" name="base_price" class="form-control" placeholder="0.00">
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <strong>Ganancia:</strong>
                    <input type="number" step="0.01" value="{{ $product->fee_gain }}"  id="fee_gain" name="fee_gain" class="form-control" placeholder="0.00">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-success">GUARDAR</button>
                </div>
            </div>
        </div>
    </div>
   
</form>
@endsection

@section('js')
    <script type='text/javascript'>
        $("#price_with_tax").focusout(function() {
            $price_with_tax = $("#price_with_tax").val();
            $valor = $price_with_tax - ($price_with_tax*0.12)
            $('#base_price').val($valor.toFixed(2));

            $factory_price = $("#factory_price").val();

            $fee_gain =  $price_with_tax - $factory_price;
            $('#fee_gain').val($fee_gain.toFixed(2));
        });
    </script>
@endsection
