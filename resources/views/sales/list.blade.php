@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-10">
                <h2>Ventas del dia</h2>
        </div>
        <div class="col-lg-2">
            <a class="btn btn-success" href="{{ route('products.create') }}">Ingresar venta</a>
        </div>
    </div>
 
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
 
    <table class="table table-dark">
        <tr>
            <th>No</th>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Fecha</th>
            <th>Total</th>
            <th>Tipo</th>
            <th width="280px">Action</th>
        </tr>
        @php
            $i = 0;
        @endphp
        @foreach ($sales as $sale)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $sale->product_id }}</td>
                <td>{{ $sale->quantity }}</td>
                <td>{{ $sale->sale_date }}</td>
                <td>{{ $sale->total }}</td>
                <td>{{ $sale->sale_type }}</td>
                <td>
                    <form action="{{ route('sales.index',$sale->id) }}" method="POST">
                        <!-- <a class="btn btn-info" href="{{ route('sales.index',$sale->id) }}">Show</a> -->
                        <a class="btn btn-primary" href="{{ route('sales.edit',$sale->id) }}">Editar</a>
                        <a class="btn btn-danger" href="{{ route('sales.delete',$sale->id) }}">Eliminar</a>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection